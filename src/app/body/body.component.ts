import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
    items: Array<any> = [];

  constructor() { 
    this.items= [
      { name: 'assets/images/1.jpg'},
      { name: 'assets/images/2.jpg'},
      { name: 'assets/images/3.jpg'},
      { name: 'assets/images/4.jpg'},
      { name: 'assets/images/5.jpg'},
    ]
  }

  ngOnInit(): void {
  }

}
