"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var LiveClassComponent = /** @class */ (function () {
    // video: any;
    function LiveClassComponent() {
    }
    LiveClassComponent.prototype.ngOnInit = function () {
        // this.video = this.videoElement;
    };
    LiveClassComponent.prototype.onSuccess = function (stream) {
        var videoArea = document.querySelector("video");
        console.log("Success! We have a strem!");
        videoArea.srcObject = stream;
    };
    ;
    LiveClassComponent.prototype.initCamera = function (config) {
        var browser = navigator;
        var constrains = { audio: true, video: true };
        browser.getUserMedia = (browser.getUserMedia ||
            browser.webkitGetUserMedia ||
            browser.mozGetUserMedia ||
            browser.msGetUserMedia);
        browser.getUserMedia(constrains, onSuccess, onError);
    };
    LiveClassComponent.prototype.onError = function (error) {
        console.log("Error with getUserMedia: ", error);
    };
    LiveClassComponent.prototype.start = function () {
        this.initCamera({ video: true, audio: true });
    };
    __decorate([
        core_1.ViewChild('videoElement')
    ], LiveClassComponent.prototype, "videoElement");
    LiveClassComponent = __decorate([
        core_1.Component({
            selector: 'app-live-class',
            templateUrl: './live-class.component.html',
            styleUrls: ['./live-class.component.css']
        })
    ], LiveClassComponent);
    return LiveClassComponent;
}());
exports.LiveClassComponent = LiveClassComponent;
